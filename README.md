# PyQBE

PyQBE is a python builder for [QBE](https://c9x.me/compile/)
[intermediate language](https://c9x.me/compile/doc/il.html).

Optionally, it can call `qbe` and `cc` to build the binary.

PyQBE aims to JIT-compile and create compiled functions on the fly,
that are callable from your python code. This feature is currently
rather limited, but [sort of works](./examples/030-python_call.py).

## Caveat Emptor

This is only tested on linux with ELF binaries. The final binary code
is emited by gcc. It might be possible to produce MacOS/Mach-O binaries,
possibly with Clang, but it's not supported. It may require some minor hacks
in PyQBE, and it has never been done, to my knowledge. I'm open to merge
requests, as long as they provide a way to test, in the cloud, on MacOS, as I
don't own a Mac myself to run the proposed code.

## What is is good for?

Nothing much at the moment, I fear. The main goal of PyQBE is to provide a
way to JIT compile code. As a proof of concept, I'm building a
PEG parser that compiles the parsing logic to machine code on the fly,
given a python definition. It's not worth showing in its current state, though.

Aside from this, one could use PyQBE to write a compiler, though I think
it's a bad idea. The second biggest strenght of QBE versus, say, LLVM,
is its compilation speed (the first being simplicity :)). Wrapping this speed
in a coat of python sluginess is a pitty.

It's not suitable for production stuff, yet. 0 test, 0 line of doc... You get it,
it's a PoC.

## What is it not?

TL;DR: PyQBE does no assumption about your semantics, and provides
no support for high level language features.

PyQBE provides low-level QBE IL AST and a few helpers to create most
common code constructs (functions, if and loops). Those helpers are
carefully designed, but we don't guarantee it's the fastest way nor
the best way to do it.

PyQBE doesn't perform any check on variable lifetime or even existence.

PyQBE will happily emit QBE IL that tries to use an undefined variable.
Hopefuly this error is most probably catch by QBE itself.

Likewise, in QBE, pointers are just 64bits long ints, with no other
semantics. PyQBE lets you allocate some stack memory, return a pointer
to this stack variable, and use this pointer at call site, while the
pointed variable is probably gone. Oooch. QBE will probably NOT catch
this error. If you're lucky enough, the kernel will, and will spit a
nice core dump at runtime.

Allocating on the heap is left to the user (although we might provide
a helper wrapping `malloc` and `free` at some point). Here again PyQBE
will happily let you:

* forget to check nulliness of a `malloc`ated pointer,
* double-`free` a pointer,
* use a pointer after `free`
* ...

... if that's realy what you want. I guess it's not, but who am I to
judge?

## Demo

```
make demo
```

This runs a bunch of python scripts in `./examples`. Note that those
scripts don't provide a lot over writting QBE IL directly, except it's
far more verbose. The user is supposed to build the tree programatically,
like we try to do in `examples/040-peg.py` (that is not included in the
demo yet but can be run using `make example-peg`).

### Low level API

PyQBE exposes low-level AST nodes to build a QBE source tree
that is compiled to QBE IL. In this example (from `examples/001-hello_world.py`),
`Function`, `Block`, `Call`, `Varargs` map directly to their respective
QBE IL nodes.


```python
from pyqbe.ast.qbe import (
        Data, Function, Return, Call,
        Word, Varargs, Block)
from pyqbe.ast.string import String
from pyqbe.program import Program
from pyqbe.compiler import Compiler

# define the main function
main = Function(
    name="main",
    params=[],
    body=[
        Block(
            # `*` in block names is replaced by a monotonic counter
            name="start*",
            body=[
                # call stdio's `printf`
                Call(
                    func=Function("printf", [], []),
                    args=[
                        Data("fmt", String('Hello!\n')),
                        Varargs]),
                # return with a return code
                Return(val=Word(0))])
    ],
    type=Word,

    # this function is exported
    export=True
)

# gather this in a unit of QBE compilation (a.k.a "Program")
prog = Program(
    main,
    name="hello_world")

comp = Compiler()
prog = comp.compile_to_qbe(prog)
print(str(prog))
```

This produces this QBE IL:

```
export function w $main(){

@start_1
	call $printf(l $fmt, ...)
	ret 0
}
data $fmt = {b "Hello!\n", b 0 }
```

You can also ask pyqbe to generate the binary:

```python
# compile the binary (by default in f".build/dest/{BIN_NAME}.bin"
# This requires qbe and cc commands to be in your $PATH
comp.compile(prog)
```

Which (hopefuly) is runable

```
❯ .build/dest/hello_world.bin
Hello!
```

### Helpers

PyQBE also exposes higher level constructs to build conditionals (`If`) and
loops (`Loop` and `Break` allow `while`, `for` and `do...until` -like
constructs)

Here's an example (form `examples/014-for_loop.py`) that creates a program
that iterates over command line arguments:

```python
from pyqbe.ast.qbe import (
        Data, Function, Return, Call, Add, Param, Long,
        Word, Varargs, Locale, Block, Copy, Load, Sub)
from pyqbe.ast.control import Loop
from pyqbe.ast.string import String

from pyqbe.program import Program
from pyqbe.compiler import Compiler


# pre-declarations of locales to avoid repetitions
iter = Locale("iter", Long)
ptr = Locale("ptr", Long)
arg = Locale("arg", Long)
argc = Locale("argc", Long)
arg_num = Locale("arg_num", Long)

# main function
main = Function(
    name="main",
    comment="Main function",
    params=[
        Param("argc", Long),
        Param("argv", Long),
        ],
    body=[
        Block(name="start", body=[

            # this loops over the command line arguments
            # This loop is equivalent to C:
            # for (int iter=argc; iter--; iter != 0) { body... }
            Loop(
                name="argv_loop",

                # init is a set of instructions that are added before the
                # loop.
                init=[
                    # ptr points to the next argument in the argv array
                    Copy(value=Locale("argv"), assign=ptr),
                    # init the iteration counter
                    Copy(value=argc, assign=iter),
                ],

                # incr instructions are added at the end of the body. Use
                # it to increment the iteration counter and setup some loop
                # local variables
                incr=[
                    # decremnent the counter
                    Sub(left=iter, right=Long(1), assign=iter),
                    # move the arg pointer by one 64bits address
                    Add(left=ptr, right=Long(8), assign=ptr)
                ],

                # the loop continues as long as the iter counter is not 0
                # the condition is evaluated at the begining of the loop
                cond=iter,

                body=[
                    # de-reference the arg pointer
                    Load(value=ptr,
                         assign=arg),
                    # compute the diplayed argument index (argc - iter)
                    Sub(left=argc, right=iter, assign=arg_num),
                    # print the pointed argument and its index
                    Call(
                        func="$printf",
                        args=[
                            Data("fmt", String("arg %d: %s\n")),
                            arg_num,
                            arg,
                            Varargs
                        ]),
                ]
            ),
            # success...
            Return(val=Word(0))])
    ],
    type=Word,

    # this function is exported
    export=True
)

prog = Program(
    main,
    name="for_loop")

comp = Compiler()
prog = comp.compile_to_qbe(prog)
print(str(prog))
```

This generates:

```
# Main function
export function w $main(l %argc, l %argv){

@start

@argv_loop_init
    %ptr =l copy %argv
    %iter =l copy %argc

@argv_loop_start
    jnz %iter, @argv_loop_cond_true, @argv_loop_cond_false

@argv_loop_cond_true
    %arg =l loadl %ptr
    %arg_num =l sub %argc, %iter
    call $printf(l $fmt, l %arg_num, l %arg, ...)
    %iter =l sub %iter, 1
    %ptr =l add %ptr, 8
    jmp @argv_loop_start

@argv_loop_cond_false
    jmp @argv_loop_end

@argv_loop_cond_end

@argv_loop_end
    ret 0
}
data $fmt = {b "arg %d: %s\n", b 0 }
```

That compiles to `.build/dest/for_loop.bin` 

```
❯ .build/dest/for_loop.bin aa bb cc
arg 0: .build/dest/for_loop.bin
arg 1: aa
arg 2: bb
arg 3: cc
```
