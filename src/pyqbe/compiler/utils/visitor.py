from dataclasses import fields, is_dataclass
from typing import get_origin

from pyqbe.ast.base import Node


class visitor:
    def __init__(self, *nodecls):
        self.nodecls = nodecls

    def __call__(self, fn):
        fn.is_visitor_of = self.nodecls
        return fn


class Visitor:
    _visitors = {}

    def __init__(self):
        self.visited = set()

    def __init_subclass__(cls):
        old_visitors = cls._visitors
        visitors = {}
        for name in dir(cls):
            if callable((meth := getattr(cls, name))):
                node_types = getattr(meth, "is_visitor_of", None)
                if node_types:
                    for node_type in node_types:
                        visitors[node_type] = meth

        cls._visitors = old_visitors | visitors

    def visit(self, n: Node):
        if id(n) in self.visited:
            return
        self.visited.add(id(n))

        mro = [c for c in n.__class__.mro() if issubclass(c, (list, Node))]
        searched = []
        for c in mro:
            if c in self._visitors:
                meth = self._visitors[c]
                for c in searched:
                    self._visitors[c] = meth
                break
            searched.append(c)
        else:
            meth = self.__class__.visit_generic

        if meth(self, n) is not False:
            self.visit_subnodes(n)

    def visit_maybe_list(self, n):
        if isinstance(n, list):
            for sub in n:
                self.visit_maybe_list(sub)
        elif isinstance(n, Node):
            self.visit(n)

    def visit_subnodes(self, n: Node):
        if not is_dataclass(n):
            return
        for f in fields(n):
            self.visit_maybe_list(getattr(n, f.name))

    def visit_generic(self, n: Node):
        return True
