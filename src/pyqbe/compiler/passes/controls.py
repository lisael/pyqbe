from collections import Counter
from dataclasses import fields, is_dataclass, Field

from ..utils.visitor import Visitor, visitor
from pyqbe.program import Program
from pyqbe.ast.base import Node
from pyqbe.ast.macro import Macro
from pyqbe.ast.qbe import (Call, Block, Jnz, Jmp, Function, Locale,
                           Word, Instruction, Return, BlockMark)
from pyqbe.ast.expression import Expression
from pyqbe.ast.control import If, Loop, Break


class DesugarControls(Visitor):
    """
    Desugar If, Loop and Macros to pure low level pyqbe.ast.qbe objects
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.func_body = None
        self.block_body = None
        self.name_counter = None
        self.current_block = None
        self.loop_end_stack = []

    def _expand_expression_field(self, n: Node, f: Field):
        """
        check if the field is an expression or (in case it's a list) if
        it contains an expression. Expand expressions if found and perform
        var substitution in the original Node
        """
        fv = getattr(n, f.name)
        if isinstance(fv, Expression):
            self.name_counter["_expr_locale_"] += 1
            loc = Locale(
                f"_expr_locale_{self.name_counter['_expr_locale_']}",
                fv.type)
            fv.assign = loc
            setattr(n, f.name, loc)
            self.visit_block_subnode(fv)
        elif isinstance(fv, list):
            result = []
            for item in fv:
                if isinstance(item, Expression):
                    self.name_counter["_expr_locale_"] += 1
                    loc = Locale(
                        f"_expr_locale_{self.name_counter['_expr_locale_']}",
                        item.type)
                    item.assign = loc
                    result.append(loc)
                    self.visit_block_subnode(item)
                else:
                    result.append(item)
            setattr(n, f.name, result)
        elif hasattr(fv, "assign") and fv.assign is None:
            setattr(n, f.name, Expression(fv))
            self._expand_expression_field(n, f)

    def expand_expressions(self, n: Node):
        """
        Check if a node accepts expressions in its fields and expand them
        as variables that are substitued in the node itself

        >>> DoStuff(right=Expression(Add(right=1, left=2), type=Word))

        becomes:

        >>> loc = Locale("_expr_locale_*", Word)
        >>> Add(right=1, left=2, assign=loc)
        >>> DoStuff(right=loc)
        """
        if not is_dataclass(n):
            return
        for f in fields(n):
            try:
                f.metadata["_accept_expr"]
            except KeyError:
                continue
            else:
                self._expand_expression_field(n, f)

    @visitor(Instruction)
    def visit_instruction(self, n: Instruction):
        if self.block_body is None:
            return False
        self.expand_expressions(n)
        self.block_body.append(n)

    @visitor(Function)
    def visit_function(self, n: Function):
        self.name_counter = Counter()
        self.func_body = []
        for b in n.body:
            self.visit_maybe_list(b)
        n.body = self.func_body
        return False

    @visitor(BlockMark)
    def visit_blockmark(self, bm: BlockMark):
        bm.block = self.current_block

    def visit_block_subnode(self, subn):
        if isinstance(subn, Macro):
            self.expand_expressions(subn)
            for n in subn():
                self.visit_block_subnode(n)
        elif isinstance(subn, list):
            for n in subn:
                self.visit_block_subnode(n)
        else:
            self.visit(subn)

    @visitor(Block)
    def visit_block(self, n: Block):
        orig_body = self.block_body
        orig_block = self.current_block
        current_block_body = []
        self.block_body = current_block_body
        self.current_block = n
        self.func_body.append(n)
        for subn in n.body:
            self.visit_block_subnode(subn)
        n.body = current_block_body
        self.block_body = orig_body
        self.current_block = orig_block
        return False

    @visitor(Loop)
    def visit_loop(self, l_: Loop):
        if l_.name:
            name = l_.name
            if "*" in name:
                self.name_counter[name] += 1
                name = name.replace("*", str(self.name_counter[name]))
        else:
            self.name_counter["loop"] += 1
            loop_num = self.name_counter["loop"]
            name = f"_loop_{loop_num}"
        if l_.init:
            init = Block(f"{name}_init", body=l_.init)
            self.visit_block(init)
        end = Block(f"{name}_end")
        self.loop_end_stack.append(end)
        loop = Block(f"{name}_start", body=[])
        body = l_.body
        if l_.incr:
            body.extend(l_.incr)
        body.append(Jmp(target=loop))
        if l_.cond:
            body = [If(
                    name=f"{name}_cond",
                    cond=l_.cond,
                    true=body,
                    false=[Break()])]
        loop.body = body
        # visisting a block adds it to the function body
        self.visit_block(loop)
        # the rest of the ending block is filled with remaining instructions
        # by the enclosing `visit_block`
        self.func_body.append(end)
        self.block_body = end.body
        self.loop_end_stack.pop()
        return False

    @visitor(Break)
    def visit_break(self, b: Break):
        self.block_body.append(Jmp(target=self.loop_end_stack[-b.idx]))

    @visitor(If)
    def visit_if(self, n: If):
        if n.name:
            name = n.name
        else:
            self.name_counter["if"] += 1
            if_num = self.name_counter["if"]
            name = f"_if_{if_num}"
        end = Block(f"{name}_end")
        body = n.true
        # don't add a jump if the last instruction is a jump already
        if (body and not isinstance(body[-1], (Jmp, Jnz, Return, Break))
                or not body):
            if n.assign is not None:
                body[-1].assign = n.assign
            body = body + [Jmp(target=end)]
        n.ift.name = f"{name}_true"
        n.ift.body = body
        body = n.false
        if (body and not isinstance(body[-1], (Jmp, Jnz, Return, Break))
                or not body):
            if n.assign is not None:
                body[-1].assign = n.assign
            body = body + [Jmp(target=end)]
        n.iff.name = f"{name}_false"
        n.iff.body = body
        if isinstance(n.cond, Locale):
            cond = n.cond
        else:
            cond = Locale(f"{name}_cond", Word)
            n.cond.assign = cond
            self.visit_block_subnode(n.cond)
        self.block_body.append(
            Jnz(
                val=cond,
                ifnz=n.ift,
                ifz=n.iff))
        # visisting a block adds it to the function body
        self.visit_block(n.ift)
        self.visit_block(n.iff)
        # the rest of the ending block is filled with remaining instructions
        # by the enclosing `visit_block`
        self.func_body.append(end)
        self.block_body = end.body
        # everything was visited yet, bloc automatic children visit.
        return False

    @visitor(Call)
    def visit_call(self, n: Call):
        self.visit_instruction(n)
        return False

    @visitor(Jmp, Jnz)
    def visit_jmp(self, n: Node):
        self.block_body.append(n)
        # block automatic children visit.
        # TODO: this doesn't permit the user to define the block right in
        #       the jump definition. We could add the block after the jump
        return False

    def __call__(self, prg: Program):
        for n in prg.body:
            self.visit_maybe_list(n)


def desugar_controls(prg: Program) -> Program:
    DesugarControls()(prg)
    return prg
