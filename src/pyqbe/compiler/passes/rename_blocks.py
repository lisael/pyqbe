from collections import Counter

from ..utils.visitor import Visitor, visitor
from pyqbe.program import Program
from pyqbe.ast.qbe import Block, Locale, Aggregate, Function, Data


class RenameBlocks(Visitor):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name_counter = Counter()

    def new_name(self, varname: str) -> str:
        self.name_counter[varname] += 1
        return varname.replace("*", str(self.name_counter[varname]))

    def _new_alias(self, alias: str, default: str) -> str:
        if alias == "":
            return self.new_name(default)
        elif "*" in alias:
            return self.new_name(alias)
        return alias

    @visitor(Locale)
    def visit_locale(self, n: Locale):
        n.alias = self._new_alias(n.alias, "_locale_*")

    @visitor(Data)
    def visit_data(self, n: Data):
        n.alias = self._new_alias(n.alias, "_data_*")

    @visitor(Aggregate)
    def visit_agg(self, n: Aggregate):
        if n.alias is not None:
            n.alias = self._new_alias(n.alias, "_aggregate_*")

    @visitor(Function)
    def visit_function(self, n: Function):
        n.name = self._new_alias(n.name, "_function_*")

    @visitor(Block)
    def visit_block(self, n: Block):
        if n.at_mark is not None:
            pass
        else:
            n.name = self._new_alias(n.name, "_block_*")

    def __call__(self, prg: Program):
        for n in prg.body:
            self.visit(n)
            self.name_counter = Counter()


def rename_blocks(prg: Program) -> Program:
    RenameBlocks()(prg)
    return prg
