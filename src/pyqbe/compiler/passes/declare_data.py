from collections import Counter

from ..utils.visitor import Visitor, visitor
from pyqbe.program import Program
from pyqbe.ast.qbe import Data, Function


class DeclareData(Visitor):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.var_counter = Counter()
        self.decls = []

    def new_var_name(self, varname):
        self.var_counter[varname] += 1
        return varname.replace("*", str(self.var_counter[varname]))

    @visitor(Data)
    def visit_data(self, n: Data):
        if n.alias == "":
            n.alias = self.new_var_name("_data_*")
        elif "*" in n.alias:
            n.alias = self.new_var_name(n.alias)
        self.decls.append(n)

    def __call__(self, prg: Program):
        for n in prg.body:
            if isinstance(n, Function):
                self.visit(n)
        prg.body.extend(self.decls)


def declare_data(prg: Program) -> Program:
    DeclareData()(prg)
    return prg
