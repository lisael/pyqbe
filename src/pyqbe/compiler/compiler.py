import math

from subprocess import Popen, PIPE
from pathlib import Path
from os import makedirs

from pyqbe.program import Program
from pyqbe.ast.qbe import Function
from .passes import declare_data, rename_blocks, desugar_controls

import logging

logger = logging.getLogger(__name__)


class Compiler:
    def __init__(
            self,
            qbe_bin="/usr/bin/qbe",
            cc_bin="/usr/bin/cc",
            build_dir=".build",
            dest_dir=None,
            debug=False,
            ):
        self.qbe = qbe_bin
        self.cc = cc_bin
        self.build_dir = Path(build_dir)
        makedirs(self.build_dir, exist_ok=True)
        self.dest_dir = (
                Path(build_dir) / "dest" if dest_dir is None else dest_dir)
        makedirs(self.dest_dir, exist_ok=True)
        self.shared = True
        self.debug = debug

    def compile_to_qbe(self, prg: Program):
        logger.info("------- Desugar controls -------")
        prg = desugar_controls(prg)
        logger.info("--- Add static data declarations ---")
        prg = declare_data(prg)
        logger.info("------- Rename --------")
        prg = rename_blocks(prg)
        logger.info("Generating QBE IL")
        if logger.getEffectiveLevel() <= logging.DEBUG:
            logger.debug("------------- QBE IL -------------")
            # logger.debug(prg.body)
            lines = str(prg).splitlines()
            # comute the number of digits of the max line number
            digits = math.floor(math.log(len(lines)) / math.log(10)) + 1
            fmt = f"{{:{digits}}} {{}}"
            for i, line in enumerate(lines):
                logger.debug(fmt.format(i+1, line))
            logger.debug("")
        return prg

    def compile(self, prg: Program):
        prg = self.compile_to_qbe(prg)
        for symb in prg.body:
            if isinstance(symb, Function):
                if (symb.export and symb.name == "main"):
                    shared = False
                    break
        else:
            shared = True

        name = prg.name + (".so" if shared else ".bin")

        output = str(self.dest_dir / name)

        logger.info("Calling qbe")
        if logger.getEffectiveLevel() <= logging.DEBUG:
            logger.debug("------------- ASM -------------")
            qbe_proc = self.qbe_proc()
            stdout, stderr = qbe_proc.communicate(str(prg))
            for line in stdout.splitlines():
                logger.debug(line)
            logger.debug("")

        qbe_proc = self.qbe_proc()
        cc_proc = self.cc_proc(output, shared, stdin=qbe_proc.stdout)
        qbe_proc.stdin.write(str(prg))
        qbe_proc.stdin.close()

        qbe_proc.wait()
        if qbe_proc.returncode != 0:
            logger.error(f"Error running `{' '.join(qbe_proc.args)}`")
            for line in qbe_proc.stderr.read().splitlines():
                logger.error(line)
            # TODO: there must be a better way to raise from a failed
            # subprocess
            raise ValueError()

        cc_proc.wait()
        if cc_proc.returncode != 0:
            logger.error(f"Error running `{' '.join(cc_proc.args)}`")
            for line in cc_proc.stderr.read().splitlines():
                logger.error(line)
            # TODO: there must be a better way to raise from a failed
            # subprocess
            raise ValueError()

        prg.dll = output
        if shared:
            logger.info("compilation done. Execute `prog.as_module()`"
                        " to use in python")
        else:
            logger.info(f"compilation done. Run {output}")

        return prg

    def qbe_proc(self):
        cmd = [self.qbe]
        return Popen(cmd, text=True, stdout=PIPE, stdin=PIPE, stderr=PIPE)

    def cc_proc(self, output: str, shared: bool, stdin=None):
        cmd = [self.cc]

        if shared:
            cmd.append("--shared")
        if self.debug:
            cmd.extend(["-Xassembler", "-g"])
        logger.info(f"Assemble with {self.cc}")
        cmd.extend([
            "-o", output,
            "-x", "assembler",
            "-"])
        logger.debug(" ".join(cmd))
        return Popen(cmd, stdout=PIPE, stdin=stdin, stderr=PIPE)
