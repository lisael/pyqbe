from types import ModuleType
import ctypes

from .ast.qbe import Function


class ModuleFunc:
    def __init__(self, fn, bin_func):
        self.fn = fn
        self.bin_func = bin_func
        self.from_ctype = self.fn.type.from_ctype
        self.params = fn.params

    def __call__(self, *args):
        if len(args) != len(self.params):
            raise TypeError(f"{self.fn.name}() takes {len(self.params)} "
                            f"positional argument but {len(args)} were given")
        _args = []
        for i, p in enumerate(self.params):
            _args.append(p.type.c_param_type(args[i]))
        return self.from_ctype(self.bin_func(*_args))


class Program:
    def __init__(self, *body, name="a", libs=None):
        self.body = list(body)
        self.libs = set(libs) if libs is not None else []
        self.name = name
        self.dll = None

    def append(self, instruction):
        self.body.append(instruction)

    def __str__(self):
        return "\n".join([o.declaration() for o in self.body])

    def as_module(self):
        lib = ctypes.CDLL(self.dll)

        mod = ModuleType(self.name)

        for symb in self.body:
            if isinstance(symb, Function) and symb.export:
                bin_func = lib[symb.name]
                bin_func.restype = symb.type.ctype
                mod.__dict__[symb.name] = ModuleFunc(symb, bin_func)
        return mod
