from dataclasses import dataclass
from functools import partial

from .qbe import Type, _Long, Long, Locale, Call, Copy
from .qbe import Store, Add, Word, Half, Byte
from .macro import Macro, transitive_macro


def zero(locale, size):
    result = []
    if not size:
        return result
    loc = Locale("_zero_ptr_*", Long)
    result.append(Copy(assign=loc, value=locale))
    for _ in range(size//8):
        result.append(Store(value=Long(0), assign=loc))
        result.append(Add(right=loc, left=8, assign=loc))
    size = size % 8
    if not size:
        return result[:-1]
    for _ in range(size//4):
        result.append(Store(value=Word(0), assign=loc))
        result.append(Add(right=loc, left=4, assign=loc))
    size = size % 4
    if not size:
        return result[:-1]
    for _ in range(size//2):
        result.append(Store(value=Half(0), assign=loc))
        result.append(Add(right=loc, left=2, assign=loc))
    size = size % 2
    if size:
        result.append(Store(value=Byte(0), assign=loc))
    return result


class Malloc(Macro):
    def __init__(self, size, assign=None, zeroed=False):
        self.assign = assign
        self.zeroed = zeroed
        self.size = size

    def __call__(self):
        assert self.assign is not None
        result = [
            Call(
                func="$malloc",
                args=[Long(self.size)],
                assign=self.assign)]
        if self.zeroed:
            result.extend(zero(self.assign, self.size))
        return result


class Free(Macro):
    def __init__(self, handle):
        self.handle = handle

    def __call__(self):
        return [
            Call(func="$free", args=[self.handle])]


@dataclass
class Pointer(_Long):
    type: Type

    @transitive_macro
    def malloc(self, assign=None):
        return Malloc(self.type.size, assign)

    @transitive_macro
    def malloc0(self, assign=None):
        return Malloc(self.type.size, assign, True)

    @transitive_macro
    def free(self, handle):
        return Free(handle)

    def definition(self):
        return str(Long)

    def __str__(self):
        return str(self._pyqbe_locale)

    def typed(self):
        return self._pyqbe_locale.typed()

    def __getattr__(self, name):
        meth = getattr(self.type, name)
        if meth is None or not getattr(meth, "_is_transitive", False):
            raise AttributeError(name)
        return meth


class SelfPointer:
    def __init__(self, addr):
        self.addr = addr
