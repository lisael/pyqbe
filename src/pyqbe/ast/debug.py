from dataclasses import dataclass
from typing import Any

from .qbe import Call, Data, Varargs, Function
from .macro import Macro
from .string import String
from .base import expr, required


@dataclass
class debug(Macro):
    var: Any = expr(required("var"))

    def __call__(self):
        fmt_type = "%d"
        if self.var.type is String:
            fmt_type = "%s"
        fmt = f"{self.var.alias}: {fmt_type}\n"
        return[
            Call(
                func="$printf",
                args=[
                    Data("_debug_fmt_*", String(fmt)),
                    self.var,
                    Varargs]),
        ]
