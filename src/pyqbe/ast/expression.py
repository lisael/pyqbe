from typing import Optional
from dataclasses import dataclass, replace, is_dataclass

from .base import Node, empty_list, required
from .qbe import Locale, Type
from .macro import Macro


@dataclass
class Expression(Macro):
    body: list[Node] = empty_list()
    assign: Optional[Locale] = None
    type: Optional[Type] = None

    def __post_init__(self):
        if isinstance(self.body, Node):
            self.body = [self.body]

    def _type_check(self):
        # First look a the assign argument. We type check when we're
        # sure self.assign is defined (see self.__call__())
        inferred = self.assign.type

        # then look at the last instruction of the expression
        last = self.body[-1]
        if hasattr(last, "assign"):
            if tp := getattr(self.body[-1], "type", None):
                if tp is not None:
                    if inferred is None:
                        inferred = tp
                    elif inferred is not tp:
                        raise(TypeError())
        # set the type if needed
        if self.type is None:
            self.type = inferred
        # otherwise, type check.
        else:
            if self.type is not inferred:
                raise(TypeError())
        if self.type is None:
            # we could not infer the type. Ask the user to type the expression
            raise TypeError(
                    "Can't infer Expression type. "
                    "Either `type` or a typed `assign` must be defined")
        if self.assign.type is None:
            # fix the assigned locale
            self.assign.type = self.type

    def __call__(self):
        if self.assign is None:
            raise ValueError("`assign` must be set")
        self._type_check()
        last = self.body[-1]
        if hasattr(last, "assign") and last.assign is None:
            # if is_dataclass(last):
                # last = replace(last, assign=self.assign)
            # else:
                # last.assign = self.assign
            last.assign = self.assign
        return self.body[:-1] + [last]
