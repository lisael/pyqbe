from dataclasses import dataclass, fields

from pyqbe.ast.qbe import (
        Aggregate, AggValue, Locale, Long, Add, Load, Store)
from .pointer import Pointer, SelfPointer
from .macro import transitive_macro, Macro


class StructMeta(type):
    registry = {}

    def __call__(cls, name, bases, attrs):
        newcls = type(name, bases, attrs)
        StructMeta.registry[newcls.__qualname__] = newcls
        print(newcls)
        return dataclass(newcls)


class StructGetField(Macro):
    def __init__(self, handle, offset, type=None, assign=None):
        self.handle = handle
        self.offset = offset
        self.assign = assign
        self.type = type

    def __call__(self):
        ptr = Locale("_struct_get_field_*_ptr", Long)
        result = [
            # TODO: optimize out if offset is 0
            Add(assign=ptr, left=self.handle, right=self.offset),
            Load(assign=self.assign, value=ptr),
            self.assign
        ]
        return result


class StructSetField(Macro):
    def __init__(self, handle, offset, type, value):
        self.handle = handle
        self.offset = offset
        self.value = value
        self.type = type

    def __call__(self):
        ptr = Locale("_struct_set_field_*_ptr", Long)
        result = [
            # TODO: optimize out if offset is 0
            Add(assign=ptr, left=self.handle, right=self.offset),
            Store(assign=ptr, value=self.value),
        ]
        return result


@dataclass
class Struct(Aggregate):

    def __init_subclass__(cls, **kwargs):
        newcls = dataclass(cls)
        newcls._offsets = {}
        offset = 0
        for f in fields(newcls):
            if f.name in ["comment", "types", "alias", "alignment", "size"]:
                continue
            if f.type is SelfPointer:
                f.type = Pointer(newcls)
                f.default = f.type(f.default.addr)
            newcls._offsets[f.name] = offset
            offset += f.type.size
        return newcls

    @property
    def size(self):
        return sum([t.size for t in self.types])

    @size.setter
    def size(self, value):
        pass

    @transitive_macro
    def new(self, handle) -> Macro:
        return Pointer(self).malloc(handle)

    @transitive_macro
    def set(self, handle, field_name, value):
        try:
            f = getattr(self, field_name)
        except Exception:
            raise
        else:
            if field_name not in self._offsets:
                raise AttributeError(field_name)
        return StructSetField(
                handle, self._offsets[field_name], type=f.type, value=value)

    @transitive_macro
    def get(self, handle, field_name, assign=None):
        try:
            f = getattr(self, field_name)
        except Exception:
            raise
        else:
            if field_name not in self._offsets:
                raise AttributeError(field_name)
        return StructGetField(
                handle, self._offsets[field_name], type=f.type, assign=assign)

    def __post_init__(self):
        super().__post_init__()
        if self.types:
            raise ValueError("`types` must not be user defined")
        for f in self.__class__._offsets:
            self.types.append(self.__class__.__dataclass_fields__[f].type)

        if self.alias is None:
            self.alias = self.__class__.__name__ + "*"

    def __call__(self, **kwargs):
        args = []
        for name in self._offsets:
            try:
                args.append(kwargs.pop(name))
            except KeyError:
                raise ValueError(f"Field `{name}` is missing")
        return StructValue(
                value=args,
                type=self)


@dataclass
class StructValue(AggValue):
    pass
