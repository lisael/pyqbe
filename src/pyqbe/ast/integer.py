import ctypes
from dataclasses import dataclass
from .qbe import _Long, _Word, _Half, _Byte


@dataclass
class _Uint64(_Long):
    ctype = ctypes.c_uint64


Uint64 = _Uint64()


@dataclass
class _Uint32(_Word):
    pass


Uint32 = _Uint32()


@dataclass
class _Uint16(_Half):
    pass


Uint16 = _Uint16()


@dataclass
class _Uint8(_Byte):
    pass


Uint8 = _Uint8()


@dataclass
class _Int64(_Long):
    ctype = ctypes.c_int64


Int64 = _Int64()


@dataclass
class _Int32(_Word):
    ctype = ctypes.c_int32


Int32 = _Int32()


@dataclass
class _Int16(_Half):
    pass


Int16 = _Int16()


@dataclass
class _Int8(_Byte):
    pass


Int8 = _Int8()
