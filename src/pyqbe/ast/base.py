from dataclasses import field
from types import MappingProxyType


class Node:
    pass


class Required:
    def __init__(self, name: str):
        self.name = name

    def __call__(self):
        raise ValueError(f"`{self.name}` is required")


def required(name: str):
    return field(default_factory=Required(name))


def empty_list():
    return field(default_factory=list)


def expr(f=None, **kwargs):
    """
    Add a marker in a field's metadata stating that this field accepts
    expressions
    """
    if f is None:
        f = field(**kwargs)
    f.metadata = MappingProxyType(f.metadata | {"_accept_expr": True})
    return f
