from typing import Union, Tuple
from dataclasses import dataclass, field
import ctypes

from .qbe import Byte, _Byte, Aggregate, AggValue, Long, Type
from .table import Table, TableValue


def _str_factory():
    return [Byte]


@dataclass
class StringValue(TableValue):
    def __post_init__(self):
        assert len(self.value) == len(self.type.types)


def identity(obj):
    return obj


@dataclass
class _String(Table):
    ctype = ctypes.c_char_p
    native_type = Long
    inner_type: Type = Byte
    table_size: int = 0

    def __post_init__(self):
        Aggregate.__post_init__(self)

    @staticmethod
    def c_param_type(param):
        if isinstance(param, str):
            return ctypes.c_char_p(param.encode("utf-8"))
        return ctypes.c_char_p(param)

    def __call__(self, string):
        return StringValue(
                value=[string, 0],
                type=self.__class__(
                    types=[(Byte, len(string.encode("utf-8"))), Byte]))

    def val_repr(self, value):
        value = value[0]
        value = value.replace("\\", "\\\\")
        value = value.replace("\n", "\\n")
        value = value.replace("\t", "\\t")
        value = value.replace('"', '\\"')
        return f'{{b "{value}", b 0 }}'

    def from_ctype(self, binobj):
        return binobj.decode("utf-8")
        return ctypes.c_char_p.from_buffer(binobj).value


String = _String()
