from dataclasses import dataclass
from typing import Optional

from .qbe import Value, Instruction, Block, Locale
from .base import Node, required, empty_list, expr


@dataclass
class If(Node):
    name: str = ""
    cond: Value = expr(required("cond"))
    true: list[Instruction] = empty_list()
    false: list[Instruction] = empty_list()
    assign: Optional[Locale] = None

    def __post_init__(self):
        self.ift = Block()
        self.iff = Block()


@dataclass
class Loop(Node):
    name: str = ""
    body: list[Instruction] = empty_list()
    init: list[Instruction] = empty_list()
    incr: list[Instruction] = empty_list()
    cond: Optional[Value] = expr(default=None)


@dataclass
class Break(Node):
    idx: int = 1
