from dataclasses import dataclass
from typing import Any

from .qbe import (
        Type, Aggregate, AggValue, Long, Locale, Copy, Sub, Load, Add, Mul)
from .base import required
from .macro import Macro, transitive_macro
from .control import Loop


class TableIterator(Macro):
    def __init__(self, handle, iterator, inner_type, table_size, body=None):
        self.handle = handle
        self.iterator = iterator
        self.inner_type = inner_type
        self.table_size = table_size
        self.body = body if body is not None else []

    def __call__(self):
        idx = Locale("_table_iter_*_idx", Long)
        ptr = Locale("_table_iter_*_ptr", Long)
        return [
            Loop(
                "table_iter_*",
                init=[
                    Copy(assign=idx,
                         value=Long(self.table_size)),
                    Copy(assign=ptr, value=self.handle),
                ],
                body=[
                    Load(assign=self.iterator, value=ptr)
                ] + self.body,
                cond=idx,
                incr=[
                    Sub(left=idx, right=Long(1), assign=idx),
                    Add(
                        assign=ptr,
                        left=ptr,
                        right=self.inner_type.size),
                ]
                ),
        ]


class TableGetItem(Macro):
    def __init__(self, handle, index, type, assign=None, ptr=None):
        self.handle = handle
        self.index = index
        self.assign = assign
        self.ptr = ptr
        self.type = type

    def __call__(self):
        offset = Locale("_table_get_*_offset", Long)
        if self.ptr is not None:
            ptr = self.ptr
        else:
            ptr = Locale("_table_get_*_ptr", Long)
        if self.type.size == 1:
            result = []
            offset = self.index
        else:
            result = [
                Mul(assign=offset, left=self.index, right=self.type.size)]
        result.extend([
            Add(assign=ptr, left=self.handle, right=offset),
            Load(assign=self.assign, value=ptr),
            self.assign
        ])
        return result


@dataclass
class TableValue(AggValue):
    def for_each(self, handle, iterator, body=None):
        return TableIterator(handle, iterator, body)


@dataclass
class Table(Aggregate):
    inner_type: Type = required("inner_type")
    table_size: int = required("size")
    native_type = Long

    def __post_init__(self):
        if self.types:
            raise ValueError("`types` must not be defined on Tables")
        self.types = [(self.inner_type, self.table_size)]

    def __call__(self, values: list[Any]) -> TableValue:
        return TableValue(
                value=values,
                type=self)

    @transitive_macro
    def for_each(self, handle, iterator, body=None):
        return TableIterator(
                handle, iterator, self.inner_type, self.table_size, body)

    @transitive_macro
    def get(self, handle, index, assign=None):
        return TableGetItem(handle, index, self.inner_type, assign=assign)
