from .base import Node


class Macro(Node):
    def __call__(self) -> list[Node]:
        raise NotImplementedError()


def transitive_macro(fn):
    fn._is_transitive = True
    return fn
