import os
import logging

from pyqbe.ast.qbe import (
        Data, Function, Return, Call, Locale,
        Word, Varargs, Block, Add)
from pyqbe.ast.string import String
from pyqbe.ast.expression import Expression
from pyqbe.ast.debug import debug

from pyqbe.program import Program
from pyqbe.compiler import Compiler

BIN_NAME = os.path.basename(__file__)[4:-3]


def main():
    result = Locale("res", Word)
    main = Function(
        name="main",
        params=[],
        body=[
            Block(
                name="start",
                body=[
                    # ( 1 + 2 ) + 3
                    # this desugars to:
                    # Add(left=1, right=2,
                    #     assing=Locale("%_expr_locale_1", Word))
                    # Add(
                    #     right=Locale("%_expr_locale_1", Word),
                    #     left=3,
                    #     assign=result)
                    Add(
                        assign=result,
                        # we wrap the first addition into an Expression
                        # with an explicit type annotation.
                        left=Expression(
                            Add(left=1, right=2),
                            type=Word),
                        right=3),
                    Call(
                        func=Function("printf", [], []),
                        args=[
                            Data("fmt", String('result is %d\n')),
                            result,
                            Varargs]),
                    # let's use debug() with an expression. This prints
                    # _expr_locale_2: 3
                    # on the terminal
                    debug(Expression(Add(left=1, right=2), type=Word)),
                    # If the type of an expression can be inferred the
                    # type annotation is not requirred. Here, by qbe inference
                    # rules on `add` we know that the result is a Word
                    debug(Expression(Add(left=Word(3), right=4))),
                    # If an unassingned assingable object is passed to a field
                    # that accepts expression it is automatically wrapped
                    # in an Expression:
                    debug(Add(left=Word(5), right=6)),
                    Return(val=Word(0))])
        ],
        type=Word,
        export=True
    )

    prog = Program(
        main,
        name=BIN_NAME)

    comp = Compiler()
    comp.compile(prog)


if __name__ == "__main__":
    r = logging.getLogger()
    r.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # fmt = logging.Formatter(
    # '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    main()
