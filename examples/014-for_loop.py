#! env python
"""
This program compiles a program that iters over its command line arguments
and prints them.

The final QBE IL is:

>>> '''
# Main function
export function w $main(l %argc, l %argv){

@start

@argv_loop_init
    %ptr =l copy %argv
    %iter =l copy %argc

@argv_loop_start
    jnz %iter, @argv_loop_cond_true, @argv_loop_cond_false

@argv_loop_cond_true
    %arg =l loadl %ptr
    %arg_num =l sub %argc, %iter
    call $printf(l $fmt, l %arg_num, l %arg, ...)
    %iter =l sub %iter, 1
    %ptr =l add %ptr, 8
    jmp @argv_loop_start

@argv_loop_cond_false
    jmp @argv_loop_end

@argv_loop_cond_end

@argv_loop_end
    ret 0
}
data $fmt = {b "arg %d: %s\n", b 0 }
'''

"""

import os
import logging

from pyqbe.ast.qbe import (
        Data, Function, Return, Call, Add, Long,
        Word, Varargs, Locale, Block, Copy, Load, Sub)
from pyqbe.ast.control import Loop
from pyqbe.ast.string import String

from pyqbe.program import Program
from pyqbe.compiler import Compiler


BIN_NAME = os.path.basename(__file__)[4:-3]


def main():
    # pre-declarations of locales to avoid repetitions
    iter = Locale("iter", Long)
    ptr = Locale("ptr", Long)
    arg = Locale("arg", Long)
    argc = Locale("argc", Long)
    argv = Locale("argv", Long)
    arg_num = Locale("arg_num", Long)

    # main function
    main = Function(
        name="main",
        comment="Main function",
        params=[argc, argv],
        body=[
            Block(name="start", body=[

                # this loops over the command line arguments
                # This loop is equivalent to C:
                # for (int iter=argc; iter--; iter != 0) { body... }
                Loop(
                    name="argv_loop",

                    # init is a set of instructions that are added before the
                    # loop.
                    init=[
                        # ptr points to the next argument in the argv array
                        Copy(value=argv, assign=ptr),
                        # init the iteration counter
                        Copy(value=argc, assign=iter),
                    ],

                    # incr instructions are added at the end of the body. Use
                    # it to increment the iteration counter and setup some loop
                    # local variables
                    incr=[
                        # decremnent the counter
                        Sub(left=iter, right=Long(1), assign=iter),
                        # move the arg pointer by one 64bits address
                        Add(left=ptr, right=Long(8), assign=ptr)
                    ],

                    # the loop continues as long as the iter counter is not 0
                    # the condition is evaluated at the begining of the loop
                    cond=iter,

                    body=[
                        # de-reference the arg pointer
                        Load(value=ptr,
                             assign=arg),
                        # compute the diplayed argument index (argc - iter)
                        Sub(left=argc, right=iter, assign=arg_num),
                        # print the pointed argument and its index
                        Call(
                            func="$printf",
                            args=[
                                Data("fmt", String("arg %d: %s\n")),
                                arg_num,
                                arg,
                                Varargs
                            ]),
                    ]
                ),
                # return with a return code
                Return(val=Word(0))])
        ],
        type=Word,

        # this function is exported
        export=True
    )

    prog = Program(
        main,
        name=BIN_NAME)

    comp = Compiler()
    comp.compile(prog)


if __name__ == "__main__":
    r = logging.getLogger()
    r.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # fmt = logging.Formatter(
    #     '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    main()
