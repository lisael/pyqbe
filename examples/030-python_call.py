#! env python
from pyqbe.ast.qbe import (
        Data, Function, Return, Call, Long,
        Locale, Block, Alloc, Add, Varargs)
from pyqbe.ast.string import String
from pyqbe.ast.integer import Int64, Int32
from pyqbe.program import Program
from pyqbe.compiler import Compiler

import logging


def build_tree():
    msg = Data("fmt", String("Hello, from QBE!"))

    # declare strdup. This is not strictly required
    strdup = Function("strdup", [Locale("s", Long)], [], type=Long)

    # just copy a static string and return a pointer that is suitable
    # for a python string backing buffer.
    say_hello = Function(
        "say_hello",
        params=[],
        body=[
            Block("start", [
                Alloc(align=4, size=4, assign=Locale("h", Long)),
                Call(func=strdup, args=[msg], assign=Locale("h", Long)),
                # return %c
                Return(Locale("h"))])
        ],
        type=String,
        export=True,
    )

    # returns an integer.
    the_answer = Function(
        "the_answer",
        params=[],
        body=[
            Block("start", [Return(val=Int64(42))])
        ],
        type=Int64,
        export=True,
    )

    # adds two integers, that are passed as python ints to the python frontend
    # function.
    a = Locale("a", Int32)
    b = Locale("b", Int32)
    c = Locale("c", Int32)
    add = Function(
        name="add",
        params=[a, b],
        body=[
            Block(body=[
                Add(
                    left=a,
                    right=b,
                    assign=c),
                Return(val=c)])
        ],
        type=Int32,
        export=True
    )

    name = Locale("name", String)
    name_size = Locale("name_size", Long)
    malloc_size = Locale("malloc_size", Long)
    result = Locale("result", Long)
    hello_to = Function(
        name="hello_to",
        params=[name],
        body=[
            Block(
                "start",
                body=[
                    Call(func=Function("strlen"), args=[name], assign=name_size),
                    Add(assign=malloc_size, left=Long(7), right=name_size),
                    Call(func=Function("malloc"), args=[malloc_size], assign=result),
                    Call(func=Function("sprintf"),
                         args=[
                             result,
                             Data("fmt2", String('Hello %s\n')),
                             name,
                             Varargs]),
                    Return(val=result)
                ])
        ],
        type=String,
        export=True
    )

    prog = Program(
        say_hello,
        the_answer,
        add,
        hello_to,
        name="hello_world")
    return prog


def compile(prog):
    comp = Compiler()
    comp.compile(prog)

    return prog


def run(prog):
    mod = prog.as_module()

    # The functions are now exposed to python code in the new module.
    print(mod.say_hello())
    print(mod.the_answer())
    print(mod.add(3, 4))
    print(mod.hello_to("€bob"))


if __name__ == "__main__":
    r = logging.getLogger()
    r.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # fmt = logging.Formatter(
    # '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    import sys
    prog = build_tree()
    if sys.argv[1] == "build":
        prog = compile(prog)
    else:
        prog.dll = sys.argv[1]
        run(prog)
