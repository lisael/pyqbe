from pyqbe.ast.qbe import (
        Data, Function, Return, Call, Add,
        Word, Varargs, Locale, Block, Ceqw, Copy)
from pyqbe.ast.control import If, Loop, Break
from pyqbe.ast.string import String

from pyqbe.program import Program
from pyqbe.compiler import Compiler

import logging


def main():
    loopidx = Locale("i", Word)

    # define the main function
    main = Function(
        "main",
        params=[],
        body=[
            Block(name="start", body=[
                Copy(assign=loopidx, value=Word(0)),
                Loop(body=[
                    Add(left=loopidx, right=Word(1), assign=loopidx),
                    Call(
                        comment="Call stdlib's printf",
                        func=Function("printf", [], []),
                        args=[
                            Data("fmt", String('Loop cycle: %d\n')),
                            loopidx,
                            Varargs]),
                    If(
                        cond=Ceqw(
                            left=loopidx,
                            right=Word(3)),
                        true=[
                            Break(),
                        ],
                    ),
                ]),
                # return with a return code
                Return(val=Word(0))])
        ],
        type=Word,

        # this function is exported
        export=True
    )

    prog = Program(
        main,
        name="loop")

    comp = Compiler()
    comp.compile(prog)


if __name__ == "__main__":
    r = logging.getLogger()
    r.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # fmt = logging.Formatter(
    #     '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    main()
