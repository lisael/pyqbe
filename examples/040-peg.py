from pyqbe.ast.qbe import (
        Data, Function, Return, Call, Add, Long, Instruction,
        Word, Varargs, Locale, Block, Value, Alloc)
from pyqbe.ast.string import String
from pyqbe.ast.struct import Struct
from pyqbe.ast.integer import Uint64
from pyqbe.ast.pointer import Pointer, SelfPointer
from pyqbe.ast.base import Node, required
from pyqbe.compiler.utils.visitor import Visitor, visitor
from pyqbe.program import Program
from pyqbe.compiler import Compiler

# https://stackoverflow.com/questions/54488765/validating-input-when-mutating-a-dataclass
from dataclasses import dataclass, field

from enum import IntFlag, auto
from typing import Optional

import logging

r = logging.getLogger()
r.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# fmt = logging.Formatter(
#     '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# ch.setFormatter(fmt)
r.addHandler(ch)


class NodeType(IntFlag):
    UNKNOWN = auto()


@dataclass
class AST:
    node_type: NodeType = NodeType.UNKNOWN
    start: int = 0
    end: int = 0


class Action:
    pass


# Our Expression base class Inherit from pyqbe's `Node` so we can
# use pyqbe `Visitor`
@dataclass
class Expression(Node):
    """
    Base class of PEG expressions

    :param:action
    :param:capture
    """
    action: Optional[Action] = None
    capture: Optional[str] = None

    def __init_subclass__(cls, /, **kwargs):
        super().__init_subclass__(**kwargs)
        newcls = dataclass(cls)
        return newcls

    def __not__(self):
        return Not(self)

    def __or__(self, other):
        if isinstance(other, Alt):
            other.exprs = [self] + other.exprs
            return other
        else:
            return Alt(exprs=[self, other])


class Exact(Expression):
    pattern: str = ""

    def __str__(self):
        pat = self.pattern
        pat = pat.replace("\\", "\\\\")
        pat = pat.replace("\n", "\\n")
        pat = pat.replace("\r", "\\r")
        pat = pat.replace("\t", "\\t")
        pat = pat.replace("\b", "\\b")
        pat = pat.replace('"', '\\"')
        return f'"{pat}"'


class Any(Expression):
    pass


class Alt(Expression):
    exprs: list[Expression] = field(default_factory=list)


class Seq(Expression):
    exprs: list[Expression] = field(default_factory=list)


class Modifier(Expression):
    expr: Optional[Expression] = None

    def __post_init__(self):
        if self.expr is None:
            raise ValueError("parameter `expr` is mandatory")


class Not(Modifier):
    pass


class Maybe(Modifier):
    pass


class Many(Modifier):
    pass


class AtLeastOne(Modifier):
    pass


class LookAhead(Modifier):
    distance: Optional[int] = 0


class Ref(Expression):
    name: str = ""
    expr: Optional[Expression] = None
    recursive: bool = False


class _ParserNode(Struct):
    start: Uint64 = Uint64(0)
    end: Uint64 = Uint64(0)


ParserNode = _ParserNode(alias="ParserNode")

ParserNodeP = Pointer(ParserNode)

StringPointer = Pointer(String(""))


class _ParserNodeList(Struct):
    node: ParserNodeP = ParserNodeP(0)
    next: SelfPointer = SelfPointer(0)


ParserNodeList = _ParserNodeList(alias="ParserNodeList")


class _Parser(Struct):
    pos: Word = Word(0)
    src: StringPointer = StringPointer(0)


Parser = _Parser(alias="Parser")


class Builder(Visitor):

    def __init__(self, rules: dict[str, Expression]):
        super().__init__()
        from collections import Counter
        self.rules = rules
        self.program = []
        self.current_func = []
        self.var_counter = Counter()
        self.failblock = None
        self.successblock = None

    def new_var_name(self, prefix):
        self.var_counter[prefix] += 1
        return f"{prefix}_{self.var_counter[prefix]}"

    @visitor(Exact)
    def visit_exact(self, n: Exact):
        block_name = self.new_var_name("exact")
        start_name = self.new_var_name("start")
        body = []
        body.extend(self.save_pos(start_name))
        block = Block(name=block_name, body=body, comment=str(n))
        self.current_func.append(block)

    @visitor(list)
    def visit_list(self, l_: list[Node]):
        for n in l_:
            self.visit(n)

    def save_pos(self, var_name: str) -> list[Instruction]:
        return [
            Add(
                right=Locale("pos"),
                left=Word(0),
                assign=Locale(var_name, Long))
        ]

    def visit_rule(self, name: str, expr: Expression):
        result = Pointer(ParserNodeList)
        self.failblock = Block(
            name="fail*",
            body=[
                Call(
                    func="$free",
                    args=[result]),
                Return(val=Long(0)),
            ])
        self.successblock = Block(
            name="success*",
            body=[
                Return(val=result)
            ])
        self.current_func = [
            Block(
                name="start",
                body=[
                    result.malloc0(),
                ])
        ]
        self.visit(expr)
        self.current_func.append(self.failblock)
        self.current_func.append(self.successblock)
        f = Function(
                name,
                params=[
                    Locale("src", Long),
                    Locale("pos", Long),
                ],
                body=self.current_func,
                type=Long,
                export=True)
        self.program.append(f)

    def add_main(self):
        body = [
            Call(
                func="$printf",
                comment="Say hello",
                args=[
                    Data("hello", String("hello world"))
                ]),

            Return(val=Long(0)),
        ]
        f = Function(
            name="main",
            params=[
                Locale("argc", Long),
                Locale("argv", Long),
            ],
            body=[
                Block(
                    name="start",
                    body=body
                )
            ],
            type=Long,
            export=True)
        self.program.append(f)

    def __call__(self) -> Program:
        # declare types first
        self.program = [
                ParserNode,
                ParserNodeList,
                Parser]
        self.add_main()
        for name, expr in self.rules.items():
            self.visit_rule(name, expr)
        return Program(*self.program)


rules = {
    "coucou": Exact(pattern="coucou")
}

prog = Builder(rules)()
prog.name = "peg"

comp = Compiler()
comp.compile(prog)

# mod = prog.as_module()

# print(mod.say_hello().decode("utf-8"))

# msg = Data("fmt", String, String("Hello, from QBE!"))

# # declare strdup. This is not strictly required

# # define the add function
# say_hello = Function(
    # "say_hello",
    # params=[],
    # body=[
        # Block("start", [
            # Alloc(4, 4, assign=Locale("h", Long)),
            # Call(strdup, msg, assign=Locale("h", Long)),
            # # return %c
            # Return(Locale("h"))])
    # ],
    # type=String,
    # # py_type=str,
    # is_global=True,
# )

# prog = Program(
    # msg,
    # say_hello,
    # name="parse")

