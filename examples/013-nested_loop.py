from pyqbe.ast.qbe import (
        Data, Function, Return, Call, Add,
        Word, Varargs, Locale, Block, Ceqw, Copy)
from pyqbe.ast.control import If, Loop, Break
from pyqbe.ast.string import String

from pyqbe.program import Program
from pyqbe.compiler import Compiler

import logging


def main():
    loop1idx = Locale("i", Word)
    loop2idx = Locale("j", Word)

    # define the main function
    main = Function(
        "main",
        params=[],
        body=[
            Block(name="start", body=[
                Copy(assign=loop1idx, value=Word(0)),
                Loop(body=[
                    Add(left=loop1idx, right=Word(1), assign=loop1idx),
                    Copy(assign=loop2idx, value=Word(0)),
                    Loop(body=[
                        Add(left=loop2idx, right=Word(1), assign=loop2idx),
                        Call(
                            func=Function("printf", [], []),
                            args=[
                                Data("fmt*", String('Loop cycle: %d - %d\n'), export= True),
                                loop1idx,
                                loop2idx,
                                Varargs]),
                        If(
                            cond=Ceqw(
                                left=loop2idx,
                                right=Word(3)),
                            true=[
                                If(
                                    cond=Ceqw(
                                        left=loop1idx,
                                        right=Word(2)),
                                    true=[
                                        Break(2),
                                    ],
                                    false=[
                                        Break()
                                    ]
                                ),
                            ],
                        ),
                    ]),
                ]),
                # return with a return code
                Return(val=Word(0))])
        ],
        type=Word,

        # this function is exported
        export=True
    )

    prog = Program(
        main,
        name="nested_loop")

    comp = Compiler()
    comp.compile(prog)


if __name__ == "__main__":
    r = logging.getLogger()
    r.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # fmt = logging.Formatter(
    #     '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    main()
