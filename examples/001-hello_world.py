import os
import logging

from pyqbe.ast.qbe import (
        Data, Function, Return, Call,
        Word, Varargs, Block)
from pyqbe.ast.string import String

from pyqbe.program import Program
from pyqbe.compiler import Compiler

BIN_NAME = os.path.basename(__file__)[4:-3]


def main():
    # define the main function
    main = Function(
        name="main",
        params=[],
        body=[
            Block(
                # `*` in block names is replaced by a monotonic counter
                name="start*",
                body=[
                    # call stdio's `printf`
                    Call(
                        func=Function("printf", [], []),
                        args=[
                            Data("fmt", String('Hello!\n')),
                            Varargs]),
                    # return with a return code
                    Return(val=Word(0))])
        ],
        type=Word,

        # this function is exported
        export=True
    )

    # gather this in a unit of QBE compilation (a.k.a "Program")
    prog = Program(
        main,
        name=BIN_NAME)

    # compile the program (by default in f".build/dest/{BIN_NAME}.bin"
    comp = Compiler()
    comp.compile(prog)


if __name__ == "__main__":
    r = logging.getLogger()
    r.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # fmt = logging.Formatter(
    # '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    main()
