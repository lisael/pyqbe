import os
import logging

from pyqbe.ast.qbe import (
        Function, Return, Word, Block, BlockMark, Phi, Locale, Sub, Jnz)

from pyqbe.program import Program
from pyqbe.compiler import Compiler

BIN_NAME = os.path.basename(__file__)[4:-3]


def main():
    iter = Locale("iter", Word)
    next_iter = Locale("next_iter", Word)
    mark = BlockMark("loop")
    startblk = Block(name="start", body=[])
    retblk = Block(name="ret", body=[Return(val=Word(0))])
    main = Function(
        name="main",
        params=[],
        body=[
            startblk,
            Block(
                name="loop",
                body=[
                    Phi(
                        assign=iter,
                        branches=[
                            (startblk, Word(100)),
                            (Block(at_mark=mark), next_iter)]),
                    Sub(assign=next_iter, left=iter, right=Word(1)),
                    mark,
                    Jnz(val=next_iter, ifnz=Block(at_mark=mark), ifz=retblk)
                ]),
            retblk
        ],
        type=Word,
        export=True
    )

    prog = Program(
        main,
        name=BIN_NAME)

    comp = Compiler()
    comp.compile(prog)


if __name__ == "__main__":
    r = logging.getLogger()
    r.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # fmt = logging.Formatter(
    # '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    main()
