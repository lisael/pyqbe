import os
import logging

from pyqbe.ast.qbe import (
        Data, Function, Return, Call, Locale,
        Word, Varargs, Block, Phi, Jmp, Copy)
from pyqbe.ast.string import String
from pyqbe.ast.control import If

from pyqbe.program import Program
from pyqbe.compiler import Compiler

BIN_NAME = os.path.basename(__file__)[4:-3]


def main():
    result = Locale("res", Word)
    var = Locale("var", Word)
    branching = If(cond=var)
    main = Function(
        name="main",
        params=[],
        body=[
            Block(
                name="start",
                body=[
                    Copy(assign=var, value=Word(1)),
                    branching,
                    Phi(assign=result, branches=[
                        (branching.ift, Word(2)),
                        (branching.iff, Word(3)),
                    ]),
                    Call(
                        func=Function("printf", [], []),
                        args=[
                            Data("fmt", String('result: %d\n')),
                            result,
                            Varargs]),
                    Return(val=Word(0))])
            ],
        type=Word,
        export=True
    )

    prog = Program(
        main,
        name=BIN_NAME)

    comp = Compiler()
    comp.compile(prog)


if __name__ == "__main__":
    r = logging.getLogger()
    r.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # fmt = logging.Formatter(
    # '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    main()
