from pyqbe.ast.qbe import (
        Data, Function, Return, Call, Add,
        Word, Varargs, Locale, Block, Ceqw, Copy)
from pyqbe.ast.control import If
from pyqbe.ast.string import String

from pyqbe.program import Program
from pyqbe.compiler import Compiler

import logging


def main():
    res = Locale("res", Word)

    # define the main function
    main = Function(
        "main",
        params=[],
        body=[
            Block(body=[
                If(
                    cond=Ceqw(
                        left=Word(1),
                        right=Word(1)),
                    true=[
                        If(
                            cond=Ceqw(
                                left=Word(1),
                                right=Word(1)),
                            true=[
                                Copy(assign=res, value=Word(1))
                            ],
                            false=[
                                Copy(assign=res, value=Word(2))
                            ],
                        ),
                    ],
                    false=[
                        Copy(assign=res, value=Word(2))
                    ],
                ),
                Call(
                    func=Function("printf", [], []),
                    args=[
                        Data("fmt", String('%d\n')),
                        res,
                        Varargs]),
                # return with a return code
                Return(val=Word(0))])
        ],
        type=Word,

        # this function is exported
        export=True
    )

    prog = Program(
        main,
        name="nested_if")

    comp = Compiler()
    comp.compile(prog)


if __name__ == "__main__":
    r = logging.getLogger()
    r.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # fmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    main()
