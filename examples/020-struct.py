import os

from pyqbe.ast.qbe import (
        Data, Function, Return, Call, Add,
        Word, Varargs, Locale, Block, Ceqw, Copy)
from pyqbe.ast.control import If, Loop, Break
from pyqbe.ast.string import String
from pyqbe.ast.struct import Struct
from pyqbe.ast.pointer import Pointer
from pyqbe.ast.debug import debug
from pyqbe.ast.expression import Expression

from pyqbe.program import Program
from pyqbe.compiler import Compiler

import logging

BIN_NAME = os.path.basename(__file__)[4:-3]


class _MyStruct(Struct):
    aa: Word = Word(0)
    bb: Word = Word(0)


MyStruct = _MyStruct()


def main():
    # create a locale of type Pointer(MyStruct)
    s = Locale("s", Pointer(MyStruct))
    bb = Locale("bb", Word)
    main = Function(
        name="main",
        comment="Main function",
        params=[],
        body=[
            Block(name="start", body=[
                # malloc a MyStruct and init the memory at 0
                s.malloc0(),
                debug(s),
                # `get()` returns the field by name and assigns its value
                # to the `assign` Locale
                s.get("bb", assign=bb),
                debug(bb),
                # `get()` is a valid expression
                debug(Add(left=s.get("aa"), right=1)),
                s.set("bb", Word(42)),
                debug(s.get("bb")),
                s.free(),
                Return(val=Word(0))])
        ],
        type=Word,
        # this function is exported
        export=True
    )

    prog = Program(
        MyStruct,
        main,
        name=BIN_NAME)

    comp = Compiler()
    comp.compile(prog)


if __name__ == "__main__":
    r = logging.getLogger()
    r.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # fmt = logging.Formatter(
    #     '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    main()
