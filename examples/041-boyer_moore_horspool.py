import os
import sys
import logging

from pyqbe.ast.qbe import (
        Function, Locale, Long, Block, Byte, Data, Return,
        Call, Sub, Cultl, Copy, Cugtl, Ceqw, Ceql, And, Word, Add,
        Varargs, Extuw
        )
from pyqbe.ast.string import String
from pyqbe.ast.table import Table
from pyqbe.ast.control import Loop, If
from pyqbe.ast.integer import Int64

# this macro (kind) of outputs the value of a local var.
from pyqbe.ast.debug import debug

from pyqbe.program import Program
from pyqbe.compiler import Compiler

BIN_NAME = os.path.basename(__file__)[4:-3]


class BMH:
    def __init__(self, pattern, name=None):
        self.name = name or "_bmh_*"
        if isinstance(pattern, str):
            pattern = pattern.encode("utf-8")
        m = len(pattern)
        skip = [m for _ in range(256)]
        for k in range(m-1):
            skip[pattern[k]] = m - k - 1

        self.skip = tuple(skip)
        self.m = m
        self.pattern = pattern

    def find_in(self, text, start=0):
        """
        The python implementation (for prototyping and easier understanding
        of the algorithm)
        """
        if isinstance(text, str):
            text = text.encode("utf-8")
        m = self.m
        skip = self.skip
        pattern = self.pattern

        n = len(text)
        k = start + m - 1
        while k < n:
            j = m - 1
            i = k
            while j > 0 and text[i] == pattern[j]:
                j -= 1
                i -= 1
            if j == 0 and text[i] == pattern[j]:
                return i
            k += skip[text[k]]
        return -1

    def declaration(self):
        text = Locale("text", String)
        # result = Locale("res", Long)
        # haystack byte size
        n = Locale("n", Long)
        # current text index
        i = Locale("i", Long)
        # current text index
        j = Locale("j", Long)
        # lookup position (end of the pattern on the haystack)
        k = Locale("k", Long)
        skip = Data(
                "_bmh_*_skip_table",
                Table(inner_type=Long, table_size=256)([self.skip]))
        pattern = Data(
                "_bmh_*_pattern",
                Table(
                    inner_type=Byte,
                    table_size=len(self.pattern)
                )([[c for c in self.pattern]])
        )
        func = Function(
            name=self.name,
            comment="Return the index of the first occurence of "
                    f"`{self.pattern.decode('utf-8')}` in the given text",
            params=[
                text,
            ],
            body=[
                Block(name="start", body=[
                    Call(
                        func="$strlen",
                        args=[text],
                        assign=n),
                    Copy(assign=k, value=self.m-1),
                    # debug(k),
                    # debug(n),
                    # debug(Cultl(left=k, right=n), type=Word),
                    Loop(
                        cond=Cultl(left=k, right=n),
                        body=[
                            Copy(assign=j, value=self.m-1),
                            Copy(assign=i, value=k),
                            Loop(
                                cond=If(
                                    cond=Cugtl(left=j, right=0),
                                    true=[
                                        If(
                                            cond=Ceqw(
                                                left=pattern.get(j),
                                                right=text.get(i)),
                                            true=[
                                                Copy(value=Word(1))
                                            ],
                                            false=[
                                                Copy(value=Word(0))
                                            ]
                                        ),
                                    ],
                                    false=[
                                        Copy(value=Word(0))
                                    ]
                                ),
                                body=[
                                    Sub(left=j, right=1, assign=j),
                                    Sub(left=i, right=1, assign=i),
                                ],
                            ),
                            If(
                                cond=Ceql(left=j, right=0),
                                true=[
                                    If(
                                        cond=Ceqw(
                                            left=pattern.get(j),
                                            right=text.get(i)),
                                        true=[
                                            Return(val=i)
                                        ])
                                ]),
                            # k += skip[text[k]]
                            Add(
                                assign=k,
                                left=k,
                                right=skip.get(
                                    Extuw(value=text.get(k))))
                        ]
                    ),
                    Return(val=Long(-1)),
                ])
            ],
            type=Int64,
            export=True
        )
        return [func]

    def find_all(self, text, start=0, overlap=False):
        result = []
        while True:
            next_res = self(text, start)
            if next_res == -1:
                return result
            else:
                result.append(next_res)
                if overlap:
                    start = next_res + 1
                else:
                    start = next_res + self.m


def main():
    finder = BMH("needle", name="find_needle")
    prog = Program(
        *finder.declaration(),
        name=BIN_NAME)

    comp = Compiler()
    # comp.compile_to_qbe(prog)
    comp.compile(prog)
    mod = prog.as_module()
    print(mod.find_needle("beedle needle?"))
    from sys import stdout
    stdout.flush()
    print(finder.find_in("beedle needle?"))


def benchmark():
    needle = b"zooplankton"
    finder = BMH(needle, name="find_zooplankton")
    prog = Program(
        *finder.declaration(),
        name="zooplankton")

    # compile the program (by default in f".build/dest/{BIN_NAME}.bin"
    comp = Compiler()
    # comp.compile_to_qbe(prog)
    comp.compile(prog)
    mod = prog.as_module()
    with open("/usr/share/dict/american-english", "rb") as f:
        haystack = f.read()
    print(mod.find_zooplankton(haystack))
    f = mod.find_zooplankton.bin_func
    param = String.c_param_type(haystack)
    import time
    t = time.time()
    for i in range(1000):
        f(param)
    print(time.time() - t)
    t = time.time()
    for i in range(1000):
        haystack.find(needle)
    print(time.time() - t)


if __name__ == "__main__":
    if len(sys.argv) == 2:
        level = logging.DEBUG
        if sys.argv[1] == "info":
            level = logging.INFO
    else:
        level = logging.DEBUG
    r = logging.getLogger()
    r.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(level)
    # fmt = logging.Formatter(
    # '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    main()
    benchmark()
