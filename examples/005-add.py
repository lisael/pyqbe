import os
import logging

from pyqbe.ast.qbe import (
        Data, Function, Return, Call, Add,
        Word, Varargs, Locale, Block)
from pyqbe.ast.string import String

from pyqbe.program import Program
from pyqbe.compiler import Compiler

BIN_NAME = os.path.basename(__file__)[4:-3]


def main():
    # pre-declaration of function locales
    a = Locale("a", Word)
    b = Locale("b", Word)
    c = Locale("c", Word)
    # define the add function
    add = Function(
        # function name
        name="add",
        # comments are added to the QBE IL to ease debuging
        comment="add two words",
        # takes two word parameters
        params=[a, b],
        # the body is a list of blocks
        body=[
            # this block has no name. It will get a generic name at
            # compilation
            Block(body=[
                # call the qbe builtin arithmetic instruction `add` and assign
                # the result to `%c`
                Add(
                    left=a,
                    right=b,
                    assign=c),
                # return %c
                Return(val=c)])
        ],
        # function type
        type=Word
    )

    # pre-declaration of a local variable in the main function
    res = Locale("res", Word)

    # define the main function
    main = Function(
        name="main",
        params=[],
        body=[
            Block(
                # `*` in block names is replaced by a monotonic counter
                name="start*",
                body=[
                    # call add with `w 1` and `w 2` and assign to res
                    Call(
                        func=add,
                        args=[Word(1), Word(2)],
                        assign=res),
                    # call stdio's `printf`
                    Call(
                        func=Function("printf", [], []),
                        args=[
                            Data("fmt", String('One and two make %d!\n')),
                            res,
                            Varargs]),
                    # return with a return code
                    Return(val=Word(0))])
        ],
        type=Word,

        # this function is exported
        export=True
    )

    # gather this in a unit of QBE compilation (a.k.a "Program")
    prog = Program(
        add,
        main,
        name=BIN_NAME)

    # compile the program (by default in f".build/dest/{BIN_NAME}.bin"
    comp = Compiler()
    comp.compile(prog)


if __name__ == "__main__":
    r = logging.getLogger()
    r.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # fmt = logging.Formatter(
    # '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    main()
