import os
import logging

from pyqbe.ast.qbe import (
        Data, Function, Return, Call,
        Word, Varargs, Block, Locale)
from pyqbe.ast.string import String
from pyqbe.ast.table import Table

from pyqbe.program import Program
from pyqbe.compiler import Compiler

BIN_NAME = os.path.basename(__file__)[4:-3]


def main():
    # create a  table type (Word[3]) and an instance
    values = Table(inner_type=Word, table_size=3)([[1, 2, 3]])
    # create a static global reference to this instance
    data = Data("values", values)

    main = Function(
        name="main",
        params=[],
        body=[
            Block(
                name="start",
                body=[
                    # use `for_each` helper method of TableValue.
                    # This returns a pyqbe.ast.macro that expands to a loop
                    # that iterates over the table.
                    data.for_each(Locale("datum", Word), body=[
                        Call(
                            func=Function("printf", [], []),
                            args=[
                                Data("fmt", String('datum: %d\n')),
                                Locale("datum", Word),
                                Varargs]),
                    ]),
                    Return(val=Word(0))])
        ],
        type=Word,
        export=True
    )

    prog = Program(
        main,
        name=BIN_NAME)

    comp = Compiler()
    comp.compile(prog)


if __name__ == "__main__":
    r = logging.getLogger()
    r.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # fmt = logging.Formatter(
    # '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # ch.setFormatter(fmt)
    r.addHandler(ch)
    main()
