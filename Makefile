PY_SRC=$(shell find src -name "*.py")
BUILD_DIR=.build/dest
PYTHON_BIN=python

$(BUILD_DIR)/add.bin: $(PY_SRC) examples/005-add.py
	$(PYTHON_BIN) examples/005-add.py

.PHONY: example-add
example-add: $(BUILD_DIR)/add.bin
	$(BUILD_DIR)/add.bin


$(BUILD_DIR)/phi.bin: $(PY_SRC) examples/006-phi.py
	$(PYTHON_BIN) examples/006-phi.py

.PHONY: example-phi
example-phi: $(BUILD_DIR)/phi.bin
	$(BUILD_DIR)/phi.bin


$(BUILD_DIR)/phi_loop.bin: $(PY_SRC) examples/007-phi_loop.py
	$(PYTHON_BIN) examples/007-phi_loop.py

.PHONY: example-phi_loop
example-phi_loop: $(BUILD_DIR)/phi_loop.bin
	$(BUILD_DIR)/phi_loop.bin


$(BUILD_DIR)/if.bin: $(PY_SRC) examples/010-if.py
	$(PYTHON_BIN) examples/010-if.py

.PHONY: example-if
example-if: $(BUILD_DIR)/if.bin
	$(BUILD_DIR)/if.bin


$(BUILD_DIR)/nested_if.bin: $(PY_SRC) examples/011-nested_if.py
	$(PYTHON_BIN) examples/011-nested_if.py

.PHONY: example-nested_if
example-nested_if: $(BUILD_DIR)/nested_if.bin
	$(BUILD_DIR)/nested_if.bin


$(BUILD_DIR)/loop.bin: $(PY_SRC) examples/012-loop.py
	$(PYTHON_BIN) examples/012-loop.py

.PHONY: example-loop
example-loop: $(BUILD_DIR)/loop.bin
	$(BUILD_DIR)/loop.bin


$(BUILD_DIR)/nested_loop.bin: $(PY_SRC) examples/013-nested_loop.py
	$(PYTHON_BIN) examples/013-nested_loop.py

.PHONY: example-nested_loop
example-nested_loop: $(BUILD_DIR)/nested_loop.bin
	$(BUILD_DIR)/nested_loop.bin


$(BUILD_DIR)/for_loop.bin: $(PY_SRC) examples/014-for_loop.py
	$(PYTHON_BIN) examples/014-for_loop.py

.PHONY: example-for_loop
example-for_loop: $(BUILD_DIR)/for_loop.bin
	$(BUILD_DIR)/for_loop.bin aa bb cc


$(BUILD_DIR)/expressions.bin: $(PY_SRC) examples/015-expressions.py
	$(PYTHON_BIN) examples/015-expressions.py

.PHONY: example-expressions
example-expressions: $(BUILD_DIR)/expressions.bin
	$(BUILD_DIR)/expressions.bin


$(BUILD_DIR)/table.bin: $(PY_SRC) examples/020-table.py
	$(PYTHON_BIN) examples/020-table.py

.PHONY: example-table
example-table: $(BUILD_DIR)/table.bin
	$(BUILD_DIR)/table.bin


$(BUILD_DIR)/struct.bin: $(PY_SRC) examples/020-struct.py
	$(PYTHON_BIN) examples/020-struct.py

.PHONY: example-struct
example-struct: $(BUILD_DIR)/struct.bin
	$(BUILD_DIR)/struct.bin


$(BUILD_DIR)/hello_world.so: $(PY_SRC) examples/030-python_call.py
	$(PYTHON_BIN) examples/030-python_call.py build

example-python_call: $(BUILD_DIR)/hello_world.so
	$(PYTHON_BIN) examples/030-python_call.py $(BUILD_DIR)/hello_world.so


$(BUILD_DIR)/peg.bin: $(PY_SRC) examples/040-peg.py
	$(PYTHON_BIN) examples/040-peg.py

.PHONY: example-peg
example-peg: $(BUILD_DIR)/peg.bin
	$(BUILD_DIR)/peg.bin


.PHONY: example-bmh
example-bmh: $(PY_SRC) examples/041-boyer_moore_horspool.py
	$(PYTHON_BIN) examples/041-boyer_moore_horspool.py info


.PHONY: demo
demo: example-add example-phi example-phi_loop example-if example-nested_if example-loop example-nested_loop example-for_loop example-expressions example-table example-struct example-python_call example-bmh

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR)
